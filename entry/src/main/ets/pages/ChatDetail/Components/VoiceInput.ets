import { VoiceRecordEnum } from '../../../models/voice'
import { emitter } from '@kit.BasicServicesKit'

@Component
struct VoiceInput {
  @Consume
  voiceState: VoiceRecordEnum
  @Consume
  asrResult: string
  @State
  list: number[] = [] // 用来生成高度的数组

  // 用来展示不同的区域内容
  aboutToAppear(): void {
    emitter.on("onBuffer", (res) => {
      this.calcHeight(res.data!.buffer)
    })
  }

  aboutToDisappear(): void {
    emitter.off("onBuffer") // 卸载事件
  }

  // 计算波峰  算出30个高度给list
  calcHeight(buffer: ArrayBuffer) {
    // buffer的长度不确定 1280 / 30 算出每个份里面的平均值 / 最大值 比例-高度 =》 高度
    // 做一个转化 将buffer转化成DataView类型
    const view = new DataView(buffer) // 转成view类型
    const arr: number[] = []
    const size = Math.floor(buffer.byteLength / 20) // 分三十份，每份的大小
    // 循环三十次
    for (let index = 0; index < buffer.byteLength; index += size) {
      let sum = 0
      // 计算每一份里面的振幅的平均值
      for (let index2 = index; index2 < index + size; index2 += 2) {
        // 每个buffer算做两个字节
        // 取值 16bit值
        const sample = view.getInt16(index2, true) // 只是一份样本 要的样本的综合
        sum += Math.abs(sample)
      }
      const height = 120 * sum / (size / 2) / 32767 // 比例  某一份的高度
      arr.push(height < 10 ? 10 : height) // 最终就是30份的高度
    }
    this.list = arr // 赋值高度
  }

  @Builder
  getDisplayContent() {
    if (this.voiceState === VoiceRecordEnum.Cancel) {
      // 取消
      Row() {

      }
      .width(100)
      .height(80)
      .borderRadius(20)
      .backgroundColor($r("app.color.danger"))
      .margin({
        left: 30
      })
    }
    else if (this.voiceState === VoiceRecordEnum.RecordIng) {
      // 正在录音
      Row({ space: 2 }) {
        ForEach(this.list, (value: number) => {
          Row()
            .height(value)
            .width(2)
            .borderRadius(1)
            .backgroundColor($r("app.color.animate_voice_color"))
        })
      }
      .justifyContent(FlexAlign.Center)
      .width(180)
      .height(80)
      .borderRadius(20)
      .backgroundColor($r("app.color.chat_primary"))
    }
    else if (this.voiceState === VoiceRecordEnum.Transfer) {
      // 转化文本
      Row() {
        Text(this.asrResult)
          .fontSize(18)
          .fontColor($r("app.color.text_primary"))
      }
      .alignItems(VerticalAlign.Top)
      .padding(10)
      .width(280)
      .height(120)
      .borderRadius(20)
      .backgroundColor($r("app.color.chat_primary"))
    }
  }

  build() {
    Stack({ alignContent: Alignment.Bottom }) {
      Column() {
        // 显示三个不同的内容
        // 取消 红色取消区域
        // 录音 中间声音波峰- 计算buffer -
        // 语音转文本  pcm封装音频转文本 speeach
        this.getDisplayContent()
        // 显示关闭和文本
        Row() {
          Row() {
            Image($r("app.media.ic_public_cancel"))
              .width(30)
              .height(30)
              .fillColor(this.voiceState === VoiceRecordEnum.Cancel ?
              $r("app.color.text_primary")
                : $r("app.color.voice_round_font_color"))
          }
          .width(70)
          .aspectRatio(1)
          .borderRadius(35)
          .justifyContent(FlexAlign.Center)
          .backgroundColor(this.voiceState === VoiceRecordEnum.Cancel ?
          $r("app.color.bottom_color")
            : $r("app.color.voice_round_color"))
          .rotate({
            angle: -10
          })
          .scale({
            x: this.voiceState === VoiceRecordEnum.Cancel ? 1.2 : 1,
            y: this.voiceState === VoiceRecordEnum.Cancel ? 1.2 : 1
          })

          Row() {
            Text("文")
              .fontSize(24)
              .textAlign(TextAlign.Center)
              .fontColor(this.voiceState === VoiceRecordEnum.Transfer ?
              $r("app.color.text_primary")
                : $r("app.color.voice_round_font_color"))
          }
          .scale({
            x: this.voiceState === VoiceRecordEnum.Transfer ? 1.2 : 1,
            y: this.voiceState === VoiceRecordEnum.Transfer ? 1.2 : 1
          })
          .width(70)
          .aspectRatio(1)
          .borderRadius(35)
          .justifyContent(FlexAlign.Center)
          .backgroundColor(this.voiceState === VoiceRecordEnum.Transfer ?
          $r("app.color.bottom_color")
            : $r("app.color.voice_round_color"))
          .rotate({
            angle: 10
          })
        }
        .width('100%')
        .justifyContent(FlexAlign.SpaceBetween)
        .padding({
          left: 40,
          right: 40
        })
        .margin({
          top: 120,
          bottom: 30
        })

        Stack() {
          Image($r("app.media.ic_public_output"))
            .width('100%')
            .height(120)
            .fillColor(this.voiceState === VoiceRecordEnum.RecordIng ?
            $r("app.color.bottom_color")
              : $r("app.color.voice_round_color"))
            .scale({
              x: 1.2
            })
          Image($r("app.media.ic_public_recorder"))
            .width(30)
            .height(30)
            .fillColor(this.voiceState === VoiceRecordEnum.RecordIng ?
            $r("app.color.voice_round_color")
              : $r("app.color.bottom_color"))
        }
        .width('100%')
      }
      .alignItems(this.voiceState === VoiceRecordEnum.Cancel ? HorizontalAlign.Start : HorizontalAlign.Center)
    }
    .height('100%')
    .backgroundColor($r("app.color.voice_back_color"))

  }
}

export default VoiceInput